#include <stdio.h>
#include<math.h>

double discriminant(double a,double b, double c){

    double disc=b*b-(4*a*c);
    return disc;
}

int main()
{
    double a,b,c,discri,x1,x2;
    printf("Enter value for a: \n");
    scanf("%lf",&a);
    printf("Enter value for b: \n");
    scanf("%lf",&b);
    printf("Enter value for c: \n");
    scanf("%lf",&c);

    if(a!=0)
    {
    discri=discriminant(a,b,c);
    
    if (discri >=0){  
    x1=(-b+sqrt(discri))/2*a;
    x2=(-b-sqrt(discri))/2*a;
    printf("The first root is %.2f.\n",x1);
    printf("The second root is %.2f.\n",x2);}

    else{
    printf("Complex root");
    }}
    else{
    printf("a cannot be zero");
    }
    return 0;
}




